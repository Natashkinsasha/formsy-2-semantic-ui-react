import React from 'react';
import Formsy from 'formsy-react-2';
import { Form } from 'semantic-ui-react';

import { removeFormsyProps } from './util';


class FormsyForm extends Form {
    render() {
        return (
            <Form {...removeFormsyProps(this.props)} as={Formsy.Form}>
                {this.props.children}
            </Form>
        );
    }
}

class FormsyInput extends React.Component {
    constructor(props) {
        super(props);
    }

    updateValue = (event) => {
        this.props.setValue(event.target.value);
    };

    render() {
        this.props.onError && this.props.onError(this.getErrorMessage());
        return (<Form.Input
            value={this.props.getValue() || ''}
            onChange={this.updateValue}
            {...removeFormsyProps(this.props)}
        />);
    }
}

class FormsyCheckbox extends React.Component {
    constructor(props) {
        super(props);
    }

    updateValue = (event) => {
        this.props.setValue(!event.target.value);
    };

    render() {
        this.props.onError && this.props.onError(this.getErrorMessage());
        return (<Form.Checkbox
            value={this.props.getValue() || false}
            onChange={this.updateValue}
            {...removeFormsyProps(this.props)}
        />);
    }
}


FormsyForm.Input = Formsy.HOC(FormsyInput);
FormsyForm.Checkbox = Formsy.HOC(FormsyCheckbox);


export default FormsyForm;