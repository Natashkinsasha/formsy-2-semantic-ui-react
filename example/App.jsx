import React, {Component} from 'react';
import 'semantic-ui-css/semantic.css';
import {Container, Segment} from 'semantic-ui-react';
import Form from '../src';


export default class App extends Component {

    render() {

        return (
            <Container>
                <Segment>
                    <Form>
                        <Form.Input name="input" label="Input"/>
                    </Form>
                </Segment>
            </Container>
        );
    }
}