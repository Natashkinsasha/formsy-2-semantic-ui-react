const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ReactRootPlugin = require('html-webpack-react-root-plugin');
module.exports = {

    entry: path.join(__dirname, '/index.jsx'),

    output: {
        path: path.resolve(__dirname, '/dist'),
        filename: 'bundle.[hash].js',
        publicPath: '/'
    },



    devtool: 'source-map',

    plugins: [
        new webpack.NoEmitOnErrorsPlugin(),
        new HtmlWebpackPlugin(),
        new ReactRootPlugin(),
    ],

    resolve: {
        extensions: ['.js', '.jsx', '.scss'],
    },

    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: [/node_modules/, /public/],

            },
            {
                test: /\.jsx$/,
                loader: 'babel-loader',
                exclude: [/node_modules/, /public/],
            },
            {
                test: /\.css$/,
                loader: 'style-loader!css-loader!autoprefixer-loader',
            },
            {
                test: /\.scss$/,
                loader: 'style-loader!css-loader!autoprefixer-loader!sass-loader',
            },
            {
                test: /\.less$/,
                loader: 'style-loader!css-loader!autoprefixer-loader!less',
                exclude: [/node_modules/, /public/],
            },
            {
                test: /\.(png|jpg|gif|svg|woff|woff2|eot|ttf)(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url-loader?limit=10000&name=[name]-[hash].[ext]',
            },
        ],
    },



    devServer: {
        contentBase: '/dist',
        historyApiFallback: true,
        port: 8080,
    }

};